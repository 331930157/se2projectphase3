<!-- // written by:Yuwei Jiang
// assisted by:Cheng Chen
// debugged by:Cheng Chen -->
<html>
<head>

</head>
<body>
  <?php
    include('DBconnect.php');
    // check system stock list
    $check_all_query = "SELECT * FROM sys_stock WHERE 1";
    $check_all_sys = mysqli_query($connect,$check_all_query);
    echo '<p><br /><h1>All Stocks: </h1><br />';
    echo '<div class="table-responsive"><table class="table table-striped">';
    echo '    <thead>
      <tr>
        <th>Name</th>
        <th>Latest Price</th>
      </tr>
    </thead>
        <tbody>';
    while($sys_stock_row = mysqli_fetch_array($check_all_sys)){
        $symbol=$sys_stock_row['symbol'];
        $realtime_qry="SELECT Time,Price,Date FROM Stocks_realtime WHERE Symbol='$symbol' ORDER BY StockID desc limit 1";
        $realtime_result = mysqli_query($connect,$realtime_qry);
        if($realtime_result==false){
            echo "Mysql realtime data query failed. ";
        }
        $realtime_row = mysqli_fetch_array($realtime_result);
        echo '<tr><td><h4><a href="stock.php?s=',$symbol,'&ch=c">',$sys_stock_row['Name'],'</a></h4></td>';
        echo '<td>',$realtime_row['Price'],'</td></tr>';
    }
    echo '</tbody></table></div></p>';
    include('avg_goog.php');
    ?>
</body>
</html>
