 <!-- // written by:Yuwei Jiang
// assisted by:Cheng Chen
// debugged by:Chenfan Xiao -->
<?php
session_start();

if(!isset($_SESSION['userid'])){
	header("Location:login.html");
	exit();
}

include('DBconnect.php');
$userid = $_SESSION['userid'];
$username = $_SESSION['username'];

$symbol=$_GET['s'];
$ope=$_GET['ope'];

//check
$check_user_stock_duplicate = "SELECT usid FROM user_stock WHERE uid=$userid AND sym='$symbol' LIMIT 1 ";
$check_stock_history = "SELECT StockID FROM Stocks_history WHERE Symbol='$symbol' LIMIT 1";
$check_stock_current = "SELECT StockID FROM Stocks_realtime WHERE Symbol='$symbol' LIMIT 1";
$check_duplicate = mysqli_query($connect,$check_user_stock_duplicate);
$check_exist = mysqli_query($connect,$check_stock_history);
// $check_count = mysqli_query($connect,$check_user_stock_duplicate);
if($ope=="add"){
    //check duplicate
    if(mysqli_fetch_array($check_duplicate)){
        echo 'You have already added ',$symbol,' before. ';
    }
    //check stock exists
    elseif(mysqli_fetch_array($check_exist)){
        $add_qry = "INSERT INTO user_stock(uid,sym)VALUES('$userid','$symbol')";
        $add_stock = mysqli_query($connect,$add_qry);
        echo 'Add ',$symbol, ' successful!';
    }
    else{
    //if not
        echo 'Stock ',$symbol,' does not exist.';
    }
}
elseif($ope=="rem"){
    //check duplicate
    if(mysqli_fetch_array($check_duplicate)){
        //remove
        $rem_qry="DELETE FROM user_stock WHERE uid=$userid AND sym='$symbol'";
        $rem_stock=mysqli_query($connect,$rem_qry);
        echo 'Remove stock ',$symbol,' successful.';
    }
    else{
    //if not
        echo 'You do not have stock ',$symbol;
    }
}
else{
    echo 'Wrong operation code.';
}

?>
