<!-- // written by:Yuwei Jiang, Cheng Chen
// debugged by:Jianing Xu -->
<?php
    session_start();
    if(isset($_SESSION['userid'])){
        include('DBconnect.php');
        $userid = $_SESSION['userid'];
        $username = $_SESSION['username'];
    }

    //stock info connector
    //mysql connector
    include('DBconnect.php');

    //get url parameters
    $symbol=$_GET['s'];
    $_SESSION['symbol']=$symbol;
    $ch = $_GET['ch'];
    if(empty($_GET['s'])&&empty($_GET['ch'])){
        header("Location: stock.php?s=goog&ch=c");
        die();
    }

    //s processing begins
    $symbol_legal=0;
    $check_all_query = "SELECT * FROM sys_stock WHERE 1";
    $check_all_sys = mysqli_query($connect,$check_all_query);
    while($sys_stock_row = mysqli_fetch_array($check_all_sys)){
        if($symbol==$sys_stock_row['symbol']){
            $symbol_legal=1;
        }
    }
    if($symbol_legal==0){
        header("Location: stock.php?s=goog&ch=c");
        die();
    }
    //s processing ends

    //ch processing begins
    if($ch=='c'){
        $qry="SELECT Time,Price,Date FROM Stocks_realtime WHERE Symbol='$symbol' ORDER BY StockID desc limit 1";
        $result = mysqli_query($connect,$qry);
        if($result==false){
            echo "Mysql query failed. ";
        }
        $rows = array();
        foreach($result as $row){
            $temp = array();
            //Values
            $temp[] = array( $row[Time]);
            $temp[] = array( $row[Price]);
            $temp[]=array($row[Date]);
        }
    }
    elseif($ch=='h'){
        $qry="SELECT Close,Date FROM Stocks_history WHERE Symbol='$symbol' ORDER BY Date desc limit 1";
        $result = mysqli_query($connect,$qry);
        if($result==false){
            echo "Mysql query failed. ";
        }
        $rows = array();
        foreach($result as $row){
            $temp = array();
            //Values
            $temp[] = array($row[Close]);
            $temp[]=array($row[Date]);
        }
    }
    else{
        echo 'Wrong ch code.';
    }
    //ch processing ends
    //get stock info
    $stock_info_qry = "SELECT Name FROM sys_stock WHERE symbol='$symbol' LIMIT 1";
    $stock_info_result = mysqli_query($connect,$stock_info_qry);
    if($result==false){
        echo "Stock info query failed. ";
    }
    $stock_info = mysqli_fetch_array($stock_info_result);
    $sname=$stock_info['Name'];

?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width">
  <title>StockPre</title>
   <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.3/css/bootstrap.min.css" rel="stylesheet">
   <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
   <script src="//netdna.bootstrapcdn.com/bootstrap/3.0.3/js/bootstrap.min.js"></script>
   <link href="https://fonts.googleapis.com/css?family=Abel|Open+Sans:400,600" rel="stylesheet" />
   <link href="default.css" rel="stylesheet" type="text/css" />

   <!--google chart javascript begins-->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
        google.charts.load('current', {packages: ['corechart', 'line']});
        google.charts.setOnLoadCallback(drawBasic);
        function drawBasic() {
            var symbol = "<?php echo $_GET['s'] ?>";
            var ch = "<?php echo $_GET['ch'] ?>";
            var jsonData = $.ajax({
                url: "getData.php?s="+symbol+"&ch="+ch,
                dataType:"json",
                async: false
            }).responseText;
            var data = new google.visualization.DataTable(jsonData);
            var showEvery = parseInt(data.getNumberOfRows() / 6);
            var options = {
                // title: 'Stock History Prices - '+symbol+' - '+ch,
                title: 'Stock Prices',
                hAxis: {
                    showTextEvery: showEvery
                },
                vAxis: {
                    format: 'currency',
                    gridlines: { count: 8 }
                },
                backgroundColor: { fill:'transparent' },
                height: 240
            };
            var chart = new google.visualization.LineChart(document.getElementById('chart_div'));
            chart.draw(data, options);
        }
        //draw with date
        function drawHisDate() {
            var symbol = "<?php echo $_GET['s'] ?>";
            var ch = "h";
            var sdate=document.getElementById('startdate').value;
            var edate=document.getElementById('enddate').value;
            var jsonData = $.ajax({
                url: "getData.php?s="+symbol+"&ch="+ch+"&sdate="+sdate+"&edate="+edate,
                dataType:"json",
                async: false
            }).responseText;
            var data = new google.visualization.DataTable(jsonData);
            var showEvery = parseInt(data.getNumberOfRows() / 6);
            var options = {
                // title: 'Stock History Prices - '+symbol+' - '+ch,
                title: 'Stock Prices',
                hAxis: {
                    showTextEvery: showEvery
                },
                vAxis: {
                    format: 'currency',
                    gridlines: { count: 8 }
                },
                backgroundColor: { fill:'transparent' },
                height: 240
            };
            var chart = new google.visualization.LineChart(document.getElementById('chart_div'));
            chart.draw(data, options);
        }
    </script>
    <!--google chart javascript ends-->

    <!--add to user button javascript begins-->
    <script>
        function loadAddDoc() {
            var symbol = "<?php echo $_GET['s'] ?>";
            var xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function() {
                if (xhttp.readyState == 4 && xhttp.status == 200) {
                    document.getElementById("addbutton").innerHTML = '<button type="button" onclick="loadRemDoc()" class="btn btn-primary" data-toggle="button" aria-pressed="false" autocomplete="off">Remove</button>';
                //   document.getElementById("Alert").innerHTML = xhttp.responseText;
                }
            };
            xhttp.open("GET", "addStock.php?s="+symbol+"&ope=add", true);
            xhttp.send();
        }
        function loadRemDoc() {
            var symbol = "<?php echo $_GET['s'] ?>";
            var xhttp = new XMLHttpRequest();
            xhttp.onreadystatechange = function() {
                if (xhttp.readyState == 4 && xhttp.status == 200) {
                    document.getElementById("addbutton").innerHTML = '<button type="button" onclick="loadAddDoc()" class="btn btn-primary" data-toggle="button" aria-pressed="false" autocomplete="off">Add</button>';
                    //   document.getElementById("Alert").innerHTML = xhttp.responseText;
                }
            };
            xhttp.open("GET", "addStock.php?s="+symbol+"&ope=rem", true);
            xhttp.send();
        }
    </script>
    <!--add to user button ends-->

</head>
<body>
<!--container fluid-->
<nav class="navbar navbar-default navbar-fixed-top"  role="navigation">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="index.php">StockPre</a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <!--left navigation begins-->
      <ul class="nav navbar-nav">
        <li><a href="index.php">Index</a></li>
        <li><a href="search.php">Search</a></li>
        <li class="active"><a href="#">Stock<span class="sr-only">(current)</span></a></li>
        <li><a href="recommend.php">Recommend</a></li>
      </ul>
      <!--left navigation ends-->

      <!--right navigation begins-->
      <ul class="nav navbar-nav navbar-right">
          <!--navigation search begins-->

        <!--navigation search ends-->

        <!--my menu begins-->
        <?php require("mymenu.php"); ?>
        <!--my menu ends-->

      </ul>
      <!--right navigation ends-->
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
<!--container fluid ends-->

<!--main container begins-->
<div class="container">
  <div class="row">
    <!--left colume begins-->
    <div class="col-md-6  col-md-offset-1 panel panel-default">
     <!--stockinfo_left begins-->
    <div id="stockinfo_left">

        <!--price div begins-->
        <div id="price">
            <div id="stockname" style="float:left">
                <?php echo '<h1>'.$sname.'</h1>'; ?>
            </div>
            <!--add to user button begins-->
            <div id="addbutton" style="float:right">
                <?php
                    // session_start();
                    if(isset($_SESSION['userid'])){
                        //check
                        $check_user_stock_duplicate = "SELECT usid FROM user_stock WHERE uid=$userid AND sym='$symbol' LIMIT 1 ";
                        $check_stock_history = "SELECT StockID FROM Stocks_history WHERE Symbol='$symbol' LIMIT 1";
                        $check_stock_current = "SELECT StockID FROM Stocks_realtime WHERE Symbol='$symbol' LIMIT 1";
                        $check_duplicate = mysqli_query($connect,$check_user_stock_duplicate);
                        $check_exist = mysqli_query($connect,$check_stock_history);
                        //check duplicate
                        if(mysqli_fetch_array($check_duplicate)){
                            //if already has
                            echo '<button type="button" onclick="loadRemDoc()" class="btn btn-primary" data-toggle="button" aria-pressed="false" autocomplete="off">Remove</button>';
                        }
                        //check stock exists
                        elseif(mysqli_fetch_array($check_exist)){
                            //if do not have
                            echo '<button type="button" onclick="loadAddDoc()" class="btn btn-primary" data-toggle="button" aria-pressed="false" autocomplete="off">Add</button>';
                        }
                        else{
                            //if not
                            echo 'Stock ',$symbol,' does not exist.';
                        }
                    }
                ?>
            </div>
            <div style="clear:both"></div>
            <!--add to user stock ends-->
            <?php
                if($ch=='c'){
                    echo $symbol,' - REALTIME DATA - <a href="stock.php?s='.$symbol.'&ch=h">History</a>';
                    echo '<h2>Current price: '.$row[Price].'</h2>';
                    echo '<h3>Update time: '.$row[Date].' '.$row[Time].' EDT</h3>';
                }
                else{
                    echo $symbol, ' - HISTORY DATA - <a href="stock.php?s='.$symbol.'&ch=c">Realtime</a>';
                    echo '<h2>Current price: '.$row[Close].'</h2>';
                    echo '<h3>Update time: '.$row[Date].' </h3>';
                }
            ?>
        </div>
        <!--price div ends-->

        <!--google chart div begins-->
        <div id="chart_div" class="margin-base-vertical"></div>
        <!--google chart div ends-->
        <?php if($ch=='h'){
            echo '
            <!--google chart date begins-->
            <div class="margin-base-vertical">
                <form action="" method="post" id="GoogleDate" class="form-horizontal" role="form"/>
                <div class="col-md-1">
                    <h5>Start Date: </h5>
                    <!--<label for="ex1">Start Date: </label>-->
                    <!--<input class="form-control" type="text" id="startdate">-->
                </div>
                <div class="col-md-3">
                    <!--<label for="ex1">Start Date: </label>-->
                    <input class="form-control" type="text" id="startdate">
                </div>
                <div class="col-md-1">
                    <h5>End Date: </h5>
                    <!--<label for="ex2">End Date: </label>-->
                    <!--<input class="form-control" type="text" id="enddate">-->
                </div>
                <div class="col-md-3">
                    <!--<label for="ex2">End Date: </label>-->
                    <input class="form-control" type="text" id="enddate">
                </div>
                <div class="col-md-2">
                    <input type="button" onclick="drawHisDate()" value="Draw" class="btn btn-success btn-sm" ></p>
                </div>
                </form>
            </div>
            <!--google chart date ends-->
        ';
        } ?>

    </div>
    <!--stockinfo_left ends-->
    </div>
    <!--left colume ends-->

    <!--right colume begins-->
    <div class="col-md-4   panel panel-default">
      <!--stockinfo_right begins-->
    <div id="stockinfo_right">
        <div id="predict_info">
            <?php if($ch=='c'){ require 'prediction_current.php'; } else{ require 'prediction_history.php'; } ?>
        </div>
    </div>
    <!--stockinfo_right ends-->
    </div>
    <!--right colume ends-->
  </div><!-- //row -->

</div>
<!--main container ends-->
</body>
</html>
