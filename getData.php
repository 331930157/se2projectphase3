<!-- // written by:Yuwei Jiang
// assisted by:Cheng Chen
// debugged by:Cheng Chen -->
<?php
	require_once('DBconnect.php');

    //URL parameter
    $symbol=$_GET['s'];
    $ch=$_GET['ch'];


    //today's date
    date_default_timezone_set('America/New_York');
    $date = date('Y-m-d');
    // echo $date;
    $year = date('Y');
    $month = date('m');
    $day = date('d');
    //remove 0
    $pattern = "/(0+)(\d+)/i";
    $replacement = "\$2";
    $month = preg_replace($pattern,$replacement,$month);
    $day = preg_replace($pattern,$replacement,$day);
    $date2 = $month.'/'.$day.'/'.$year;
    // echo $date2;

    //check sdate
    if(!empty($_GET['sdate'])){
        $start_date=$_GET['sdate'];
        $start_date=strtotime($start_date);
        $start_date = date('Y-m-d', $start_date);
    }
    else{
        //calculate one year before
        $start_date = strtotime('-1 year', strtotime($date));
	    $start_date = date('Y-m-d', $start_date);
    }

    //check edate
    if(!empty($_GET['edate'])){
        $end_date=$_GET['edate'];
        $end_date=strtotime($end_date);
        $end_date = date('Y-m-d', $end_date);
    }
    else{
        $end_date=$date;
    }

    // echo $start_date;

    // echo $end_date;

     if($ch=='c'){
        $qry = "SELECT Time, Price FROM Stocks_realtime WHERE Symbol='$symbol' AND Date='$date2' ORDER BY StockID";
        $result = mysqli_query($connect,$qry);
        $watchdog=0;
        while($result->num_rows<1){
            $day=$day-1;
            $date2 = $month.'/'.$day.'/'.$year;
            $qry = "SELECT Time, Price FROM Stocks_realtime WHERE Symbol='$symbol' AND Date='$date2' ORDER BY StockID";
            $result = mysqli_query($connect,$qry);
            $watchdog++;
            if($watchdog>15){
                die("No data for this stock. ");
            }
        }
     }
     else{

        $qry= "SELECT Date, Close FROM Stocks_history WHERE Symbol='$symbol' AND Date>='$start_date' AND Date<='$end_date' ORDER BY Date";
        $result = mysqli_query($connect,$qry);
     }



	 if($result==false){
	 	echo "Mysql query failed. ";
	 }
	 mysqli_close($connect);

	$table = array();
	$table['cols'] = array(
    	//Labels for the chart, these represent the column titles
   		array('id' => '', 'label' => 'Date', 'type' => 'string'),
    	array('id' => '', 'label' => 'Price', 'type' => 'number')
    	);

	$rows = array();
	foreach($result as $row){
    	$temp = array();

    	//Values
        if($ch=='c'){
            $temp[] = array('v' => (string) $row[Time]);
            $temp[] = array('v' => (float) $row[Price]);
        }
    	else{
            $temp[] = array('v' => (string) $row[Date]);
            $temp[] = array('v' => (float) $row[Close]);
        }

    	$rows[] = array('c' => $temp);
    	}

	$result->free();

	$table['rows'] = $rows;

	$jsonTable = json_encode($table,true);
	echo $jsonTable;


?>
