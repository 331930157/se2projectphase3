<!-- // written by:Yuwei Jiang, Cheng Chen
// debugged by:Chenfan Xiao -->
<?php
session_start();
if(isset($_SESSION['userid'])){
    include('DBconnect.php');
    $userid = $_SESSION['userid'];
    $username = $_SESSION['username'];
}

?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width">
  <title>StockPre</title>
   <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.3/css/bootstrap.min.css" rel="stylesheet">
   <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
   <script src="//netdna.bootstrapcdn.com/bootstrap/3.0.3/js/bootstrap.min.js"></script>
   <link href="https://fonts.googleapis.com/css?family=Abel|Open+Sans:400,600" rel="stylesheet" />
   <link href="default.css" rel="stylesheet" type="text/css" />
</head>
<body>
<!--container fluid-->
<nav class="navbar navbar-default navbar-fixed-top"  role="navigation">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="index.php">StockPre</a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <!--left navigation begins-->
      <ul class="nav navbar-nav">
        <li><a href="index.php">Index</a></li>
        <li><a href="search.php">Search</a></li>
        <li><a href="stock.php">Stock</a></li>
        <li class="active"><a href="#">Recommend <span class="sr-only">(current)</span></a></li>
      </ul>
      <!--left navigation ends-->

      <!--right navigation begins-->
      <ul class="nav navbar-nav navbar-right">
          <!--navigation search begins-->

        <!--navigation search ends-->

        <!--my menu begins-->
        <?php require("mymenu.php"); ?>
        <!--my menu ends-->

      </ul>
      <!--right navigation ends-->
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
<!--container fluid ends-->

<!--main container begins-->
<div class="container">
  <div class="row">

    <!--main colume begins-->
    <div class="col-md-10 col-md-offset-1 panel panel-default">
        <!--put your stuff here-->
        <?php include 'recommend_content.php' ?>
    </div>
    <!--main colume ends-->

  </div><!-- //row -->
</div>
<!--main container ends-->
</body>
</html>
