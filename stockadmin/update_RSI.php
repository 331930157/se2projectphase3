<!-- // written by:Xinyu Li
// assisted by:Cheng Chen
// debugged by:Chenfan Xiao -->
<?php

require 'DBconnect.php';

ignore_user_abort(true);
set_time_limit(0);
date_default_timezone_set('America/New_York');
while(1){

// require 'DBconnect.php';

    $date = date('Y-m-d');

    $sys_stock_qry = "SELECT symbol FROM sys_stock WHERE 1";
    $sys_stock_result = mysqli_query($connect,$sys_stock_qry);
    while($sys_stock_row = mysqli_fetch_array($sys_stock_result)){
    //     $RSI_14 = getRSI($sys_stock_row['symbol']);
    //     // echo $sys_stock_row['symbol'];
    //     echo $RSI_14;
    // }
    // echo getRSI('goog');

        $symbol=$sys_stock_row['symbol'];
        // $symbol='goog';
        $sql = "SELECT * FROM Stocks_history WHERE Symbol='$symbol' LIMIT 15";
        $result = $connect->query($sql);


        $price = array();
        $i = 0;
        if ($result->num_rows > 0) {
            // output data of each row
            while($row = $result->fetch_assoc()) {
                $price[] = $row[Close];
                $i++;
            // echo "symbol: " . $row[Symbol] . " - close: " . $row[Close] ."<br>";
            //var_dump($row);
            //echo "<br>";
            }
        }
        else {
            echo $symbol." has 0 results";
        }

        $pre_price=getRSI($price);
        // echo $pre_price;
        if($pre_price>0){
            $RSI_duplicate_qry="SELECT * FROM Stocks_his_pre WHERE Symbol='$symbol' AND Date='$date' LIMIT 1";
            $RSI_duplicate=mysqli_query($connect,$RSI_duplicate_qry);
            if($RSI_duplicate->num_rows<=0){
                $RSI_Save_qry="INSERT INTO Stocks_his_pre (Symbol,Date,RSI) VALUES ('$symbol','$date',$pre_price)";
                $RSI_Save_result = mysqli_query($connect,$RSI_Save_qry) or die('cannot save RSI '.$connect->error);
            }
        }

    }//each stock ends


 sleep(3600);

}//while ends

function getRSI($price){
    $price_positive = array();
    $price_negative = array();
    for ($x=0; $x<14; $x++) {
    $a = $price[$x] - $price[$x+1];
    if($a>0){
        $price_positive[] = $a;
    }
    else{
        $price_negative[] = $a;
    }
    //echo $a . "  ";
    }

    $price1 = 0;
    for($x=0; $x<count($price_positive);$x++){
        //echo $price_positive[$x] . "<br>";
        $price1 += $price_positive[$x];
    }

    $price2 = 0;
    for($x=0; $x<count($price_negative);$x++){
        //echo $price_negative[$x] . "<br>";
        $price2 -= $price_negative[$x];
    }
    //echo $price1 . "  " . $price2 . "<br>";

    $RSI = ($price1/($price1+$price2)) * 100;
    return $RSI;
 }//getRSI ends
        $connect->close();
?>
