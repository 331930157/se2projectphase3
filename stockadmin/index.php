<!-- // written by:Yuwei Jiang
// assisted by:Cheng Chen
// debugged by:Chenfan Xiao -->
<?php

    session_start();
    if(!isset($_SESSION['userid'])){
        echo 'Please log in first. ';
        echo '<script language="javascript">history.go(-1);</script>';
        $userid = $_SESSION['userid'];
        $username = $_SESSION['username'];
        if($userid>10){
            echo 'Unauthorized user. ';
            echo '<script language="javascript">history.go(-1);</script>';
        }
    }
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>Admin - System Stock Manager</title>
<!-- Bootstrap -->
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css" rel="stylesheet">
<link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.css" rel="stylesheet" />
<link href="https://fonts.googleapis.com/css?family=Abel|Open+Sans:400,600" rel="stylesheet" />
<link href="adminmanage.css" rel="stylesheet" type="text/css" />
<!--Bootstrap ends-->
</head>

<body>
    <div class="container">
		<div class="row">
			<div class="col-md-6 col-md-offset-3 panel panel-default">
                <p class="text-center"><h1>Admin</h1></p>
                <p>
                    <a href="addsysstock.php">Manage Stock List</a><br />
                    <a href="update_current.php" target="_blank">Update Realtime Database</a><br />
                    <a href="update_history.php" target="_blank">Update History Database</a><br />
                    <a href="update_RSI.php" target="_blank">Update RSI Database</a><br />
                </p>
        <span><a href="index.php">Admin  </a>|<a href="../index.php">  Index</a></span>
    </div>
<!--panel ends-->
</div>
<!--row ends-->
</div>
<!--container ends-->
</body>
</html>
